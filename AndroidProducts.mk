#
# Copyright (C) 2021 StatiXOS
# Copyright (C) 2021 Vaisakh Murali
#
# SPDX-License-Identifer: GPL-2.0-only

#
# AOSP Lunch specification
#

PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/lineage_cannon.mk
